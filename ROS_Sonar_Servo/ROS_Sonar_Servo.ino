#include <ros.h>
#include <mini_bot/RangeSensor.h>
#include <mini_bot/MotorControl.h>
#include <mini_bot/map_sonar.h>
#include <Servo.h>

// initialise L298N controller pins
#define enA 6
#define in1 4
#define in2 5

#define enB 3
#define in3 7
#define in4 8

//reduce the max subscriber and publishers to 3 and 
//subcriber buffer = 80 / publisher or Service server buffer = 300 ( for publish map data array)
ros::NodeHandle_<ArduinoHardware, 3, 3, 80, 300> nh;
//ros::NodeHandle  nh;
mini_bot::RangeSensor range;
using mini_bot::map_sonar;

Servo servo;
// defines pin numbers
const int trigPin = 11;
const int echoPin = 10;

// defines variables
long sduration;
int sdistance;
int distance;

//sonar map service server callback function
void SonarMapData(const map_sonar::Request & req, map_sonar::Response & res) {
  MotorDirPWM(LOW,LOW,0,LOW,LOW,0);
  int k =0;
  delay(100);
  if (req.reqMap == true) {
    for (int j = 0; j < 180; j = j + 3) {
      servo.write(j);
      res.distMap[k] = SonarDistance();
      k++;
      delay(100);
    }
  }
  delay(100);
}

// motor speed control subscriber call back function
void ControlDirSpeed( const mini_bot::MotorControl& control_msg) {
  MotorDirPWM(control_msg.LWheelDir1, control_msg.LWheelDir2, control_msg.LSpeed, control_msg.RWheelDir1, control_msg.RWheelDir2, control_msg.RSpeed);
}

//initialise "sonar_distance_pub" publisher which is publishing to the "sonar_distance" topic
ros::Publisher sonar_distance_pub("sonar_distance", &range);
//motor speed subscriber
ros::Subscriber<mini_bot::MotorControl> speed_dir_cont("motor_control", &ControlDirSpeed );
//map data service server
ros::ServiceServer<map_sonar::Request, map_sonar::Response> map_server("create_map", &SonarMapData);

void setup()
{
  nh.initNode();
  nh.advertise(sonar_distance_pub);
  nh.subscribe(speed_dir_cont);
  nh.advertiseService(map_server);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  servo.attach(9);
  //configure L298D controlling pins
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
}

void loop()
{
  SonarRFLDistance();
  sonar_distance_pub.publish( &range );
  nh.spinOnce();
  delay(10);
}

//sonar distance calculator for left30 = 120, front = 90 and right30 = 120 angles
int SonarRFLDistance() {
  for (int i = 60; i < 121; i = i + 30) {
    servo.write(i);

    switch (i) {

      case 60:
        range.Right30Dist = SonarDistance();
        break;

      case 90:
        range.FrontDist = SonarDistance();
        break;

      case 120:
        range.Left30Dist = SonarDistance();
        break;

    }
    delay(500);
  }
  delay(200);
}

//sonar range finder
int SonarDistance() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  sduration = pulseIn(echoPin, HIGH);
  sdistance = sduration * 0.034 / 2;
  return sdistance;
}

//motor PWM applying function
void MotorDirPWM(boolean L1, boolean L2, int LPWM, boolean R1, boolean R2, int RPWM) {
  // Set Motor A dir     back
  digitalWrite(in1, L1); //HIGH
  digitalWrite(in2, L2); //LOW
  // Set Motor B dir
  digitalWrite(in3, R1);//HIGH
  digitalWrite(in4, R2);//LOW
  //Set motor speeds
  analogWrite(enA, LPWM); // Send PWM signal to motor A
  analogWrite(enB, RPWM); // Send PWM signal to motor B
  delay(300);
}
